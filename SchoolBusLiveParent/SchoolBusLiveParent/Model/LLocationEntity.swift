//
//  LLocationEntity.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 15/08/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class LLocationEntity: NSObject, MKAnnotation {
    
    var _bearing: Int! = 0;
    var _lat: Double! = 0.0;
    var _lng: Double! = 0.0;
    var _dateReported: String! = ""
    var coordinate: CLLocationCoordinate2D
    
    override init() {
    
        coordinate = CLLocationCoordinate2DMake(0, 0)
        super.init()
    
    }
    
    init (bearing bearing_: Int!, lat lat_: Double!, lng lng_: Double!, dateReported deteReported_:String!, coordinate coordinate_:CLLocationCoordinate2D!) {
        
        _bearing = bearing_
        _lat = lat_
        _lng = lng_
        _dateReported = deteReported_
        coordinate = coordinate_
        
    }
    
    func setCoordinate(lat:CLLocationDegrees, lon:CLLocationDegrees) -> Void {
        
        coordinate = CLLocationCoordinate2DMake(lat, lon)
        
    }

}
