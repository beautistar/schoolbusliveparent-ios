//
//  LocationEntity.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 8/7/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class LocationEntity : NSObject, MKAnnotation{
    
    var _title:String! = ""
    var _addrTypeId:Int = 0;
    var _addrLine1:String! = ""
    var _addrLine2:String! = ""
    var _lat:Double! = 0.0
    var _lon:Double! = 0.0
    var _dataOccurred:String! = ""
    var coordinate: CLLocationCoordinate2D
    
    
    override init() {
    
        coordinate = CLLocationCoordinate2DMake(0, 0)
        super.init()
    }
    
    init (title pTitle: String!, addrTypeId pAddrTypeId: Int!, _addrLine1 pAddrLine1: String!, addrLine2 pAddrLine2: String!, lat pLat:Double!, lon pLon:Double!, dataOccured pDataOccured : String!, coordinate coordinate_:CLLocationCoordinate2D!) {
        
        _title = pTitle
        _addrTypeId = pAddrTypeId
        _addrLine1 = pAddrLine1
        _addrLine2 = pAddrLine2
        _lat = pLat
        _lon = pLon
        _dataOccurred = pDataOccured
        coordinate = coordinate_
        
    }
    
    func setCoordinate(lat:CLLocationDegrees, lon:CLLocationDegrees) -> Void
    {
        
        coordinate = CLLocationCoordinate2DMake(lat, lon)
    }
}
