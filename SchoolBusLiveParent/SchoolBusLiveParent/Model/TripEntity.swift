//
//  TripEntity.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 8/7/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit

class TripEntity: NSObject {
    
    var _id: Int! = 0;
    var _tGuid: String! = ""
    var _pGuid: String! = ""
    var _tStatusId: Int! = 0
    var _company: String! = ""
    var _tDateStarted:String! = ""
    var _tDateEnded: String! = ""
    
    var _startDate: String! = ""
    
    var _pickUp:LocationEntity!
    var _dropOff:LocationEntity!
    
    var _lastLocation:LLocationEntity!
    
    
    
    override init() { }
    
    init (tGuid tGuid_: String!, pGuid pGuid_: String!, tStatusId tStatusId_: Int!, company company_: String, tDateStarted tDateStarted_: String, tDateEnded tDateEnded_:String!, startDate startDate_:String!, pickUp pickUp_:LocationEntity, dropOff dropOff_:LocationEntity, lastLocation lastLocation_:LLocationEntity) {
        
        _tGuid = tGuid_
        _pGuid = pGuid_
        _tStatusId = tStatusId_
        _company = company_
        _tDateStarted = tDateStarted_
        _tDateEnded = tDateEnded_
        
        _startDate = startDate_
        
        _pickUp = pickUp_
        _dropOff = dropOff_
        _lastLocation = lastLocation_
        
    }
}
