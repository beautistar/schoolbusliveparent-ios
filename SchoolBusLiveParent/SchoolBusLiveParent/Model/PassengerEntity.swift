//
//  PassengerEntity.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 12/08/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit

class PassengerEntity: NSObject {
    
    var _id: Int! = 0
    var _guid: String! = ""
    var _status:Int! = 0;
    var _fullName:String! = ""
    var _gender: String! = ""
    var _dob:String! = ""
    var _phone:String! = ""
    var _picurl:String! = ""
    

    override init() { }
    
    init (guid guid_: String!, status status_: Int!, fullName fullName_:String!, gender gender_:String!, dob dob_:String!, phone phone_:String!, picurl picutl_:String!) {
        

        _guid = guid_
        _status = status_
        _fullName = fullName_
        _gender = gender_
        _dob = dob_
        _phone = phone_
        _picurl = picutl_
        
    }


}
