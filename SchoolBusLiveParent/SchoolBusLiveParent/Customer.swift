//
//  Customer.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 8/4/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit

class CustomerEntity: NSObject {
        
    var _guid: String! = nil
    var _status: Int! = 0
    var _fullname: String! = nil
    var _email: String! = nil
    var _phone:String! = nil
    var _isLoggedIn: Int = 0
        
    
    override init() { }
    
    init (guid customerGuid: String!, status customerStatus: Int!, fullName customerFullName: String!, email customerEmail: String!, phone customerPhone:String!, isLoggedIn customerLogedIn: Int!) {
    
        _guid = customerGuid
        _status = customerStatus
        _fullname = customerFullName
        _email = customerEmail
        _phone = customerPhone
        _isLoggedIn = customerLogedIn        
        
    }
    
    func saveCustomerInfo() {
        
        let ud: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        ud.setObject(self._guid, forKey: "Customer_Guid")
        ud.setInteger(self._status, forKey: "Customer_Status")
        ud.setObject(self._fullname, forKey: "Customer_FullName")
        ud.setInteger(self._isLoggedIn, forKey: "Is_LoggedIn")
        ud.setObject(self._phone, forKey: "Customer_Phone")
        ud.setObject(self._email, forKey: "Customer_Email")
        ud.synchronize()
    }
    
    func setLogin(isLoggedIn: Int) {
        
        let ud: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        ud.setInteger(self._isLoggedIn, forKey: "Is_LoggedIn")
        ud.synchronize()
    }
    
    class func loadCustomerInfo() -> CustomerEntity! {
        
        let customerInfo: CustomerEntity! = CustomerEntity()
        
        let ud: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        customerInfo._guid = ud.objectForKey("Customer_Guid") as! String!
        customerInfo._status = ud.integerForKey("Customer_Status")
        customerInfo._fullname = ud.objectForKey("Customer_FullName") as! String!
        customerInfo._email = ud.objectForKey("Customer_Guid") as! String!
        customerInfo._phone = ud.objectForKey("Customer_Email") as! String!
        customerInfo._isLoggedIn = ud.integerForKey("Is_LoggedIn")
        
        return customerInfo
    }
    

}
