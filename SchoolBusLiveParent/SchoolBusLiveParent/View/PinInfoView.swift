//
//  PinInfoView.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 14/08/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import MapKit

class PinInfoView: MKAnnotationView {
    
    static let popupViewSize = CGSizeMake(305, 135)
    
    private static let shadowPadding: CGFloat = 3.0
    private static let cornoerRadius: CGFloat = 15.0
    private static let headerHieght : CGFloat = 40.0
    private static let innerPadding : CGSize = CGSizeMake(10, 10)
    static let arrowSize    : CGSize = CGSizeMake(18, 11)
    
    
    var arrowPoint: CGPoint!
    var parentView: UIView!
    var delegate: PinInfoViewDelegate?
    
    //outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddressLine1: UILabel!
    @IBOutlet weak var lblAddressLine2: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    
    
    func setLocationEntity(locationEntity: LocationEntity) -> Void {
        // TODO: implement location pin
        self.lblTitle.text = locationEntity._title
        self.lblAddressLine1.text = locationEntity._addrLine1
        self.lblAddressLine2.text = locationEntity._addrLine2
        self.lblDateTime.text = locationEntity._dataOccurred
    }
    
    static func radians(degress: Double) -> CGFloat {
        return CGFloat(degress * M_PI) / 180
    }
    
    static func CGPointOffset(point: CGPoint, dx: CGFloat, dy: CGFloat) -> CGPoint {
        return CGPointMake(point.x + dx, point.y + dy)
    }
    
    static func CGPointOffsetByPoint(originPoint: CGPoint, offsetPoint: CGPoint) -> CGPoint {
        return CGPointOffset(originPoint, dx: offsetPoint.x, dy: offsetPoint.y)
    }
    
    static func showInView(parentView: UIView, pinFrame: CGRect, delegate: PinInfoViewDelegate?, object: LocationEntity) -> PinInfoView {
        for subview in parentView.subviews {
            if subview.isKindOfClass(PinInfoView) {
                subview.removeFromSuperview()
                break
            }
        }
        
        let ox: CGFloat = CGRectGetMidX(pinFrame)
        let oy: CGFloat = pinFrame.origin.y
        let padding: CGFloat = 5.0
        var xx: CGFloat = ox - popupViewSize.width * 0.5
        
        if xx < padding {
            xx = padding
        } else if xx > CGRectGetWidth(parentView.frame) - popupViewSize.width - padding * 0.3 {
            xx = CGRectGetWidth(parentView.frame) - popupViewSize.width - padding * 0.3
        }
        
        let frame = CGRectMake(xx, oy - popupViewSize.height, popupViewSize.width, popupViewSize.height)
        let view = NSBundle.mainBundle().loadNibNamed("PinInfoView", owner: self, options: nil)[0] as! PinInfoView
        view.frame = frame
        parentView.addSubview(view)
        view.setLocationEntity(object)
        view.arrowPoint = CGPointMake(ox - shadowPadding - xx, oy)
        view.showWithAnimate(true)
        view.delegate = delegate
        
        
        return view
    }
    
    static func hideFromView(parentView: UIView, animate: Bool) {
        for subview in parentView.subviews {
            if subview.isKindOfClass(PinInfoView) {
                (subview as! PinInfoView).hideWithAnimate(true)
                break
            }
        }
        
    }
    
    func showWithAnimate(animate: Bool) -> Void {
        self.alpha = 0.5
        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.5, 0.5)
        UIView.animateWithDuration(0.1, animations: {
            self.alpha = 1
            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1)
        }) { (finished: Bool) in
            UIView.animateWithDuration(0.1, animations: {
                self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)
            }) { (finished: Bool) in
                self.delegate?.PinInfoViewDidShow(self)
            }
        }
    }
    
    func hideWithAnimate(animate: Bool) -> Void {
        UIView.animateWithDuration(0.2, animations: {
            self.alpha = 0
        }) { (finished: Bool) in
            self.removeFromSuperview()
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        //        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let context = UIGraphicsGetCurrentContext()
        let bigBoxInnerShadowColor = UIColor(white: 1, alpha: 0.5)
        let backgroundLightColor = UIColor(colorLiteralRed: 233.0/255.0, green: 246.0/255.0, blue: 249.0/255.0, alpha: 1)
        let boxStroke = UIColor.whiteColor()
        
        /*
         let darkColor = UIColor(white: 1, alpha: 1)
         let lightColor = UIColor(white: 1, alpha: 1)
         
         let gradient2Colors = [darkColor.CGColor, lightColor.CGColor]
         let gradient2Locations: [CGFloat] = [0, 1]
         let gradient2 = CGGradientCreateWithColors(colorSpace, CFArray?, <#T##locations: UnsafePointer<CGFloat>##UnsafePointer<CGFloat>#>)
         */
        
        let bigBoxInnderShadow = bigBoxInnerShadowColor.CGColor
        let bigBoxInnerShadowOffset = CGSizeMake(0, 1)
        let bigBoxInnerShadowBlurRadius: CGFloat = 1
        
        let backgroundShadow = UIColor.grayColor().CGColor
        let backgroundShadowOffset = CGSizeMake(1, 1)
        let backgroundShadowBlurRadius: CGFloat = 5
        
        let boxBounds = CGRectMake(0, 0, self.bounds.size.width - PinInfoView.arrowSize.height * 0.5, self.bounds.size.height - PinInfoView.arrowSize.height)
        
        let roundedRectanglePath = PinInfoView.createBezierPathForSize(boxBounds.size, arrowPosition: arrowPoint)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, backgroundShadowOffset, backgroundShadowBlurRadius, backgroundShadow)
        
        boxStroke.setStroke()
        roundedRectanglePath.lineWidth = 1
        roundedRectanglePath.stroke()
        backgroundLightColor.setFill()
        roundedRectanglePath.fill()
        
        var roundedRectangleBorderRect = CGRectInset(roundedRectanglePath.bounds, -bigBoxInnerShadowBlurRadius, -bigBoxInnerShadowBlurRadius)
        roundedRectangleBorderRect = CGRectOffset(roundedRectangleBorderRect, -bigBoxInnerShadowOffset.width, -bigBoxInnerShadowOffset.height)
        roundedRectangleBorderRect = CGRectInset(CGRectUnion(roundedRectangleBorderRect, roundedRectanglePath.bounds), -1, -1)
        
        let roundedRectangleNegativePath = UIBezierPath(rect: roundedRectangleBorderRect)
        roundedRectangleNegativePath.appendPath(roundedRectanglePath)
        roundedRectangleNegativePath.usesEvenOddFillRule = true
        
        CGContextSaveGState(context)
        
        let xOffset = bigBoxInnerShadowOffset.width + round(roundedRectangleBorderRect.size.width)
        let yOffset = bigBoxInnerShadowOffset.height
        CGContextSetShadowWithColor(context, CGSizeMake(xOffset + copysign(0.1, xOffset), yOffset + copysign(0.1, yOffset)), bigBoxInnerShadowBlurRadius, bigBoxInnderShadow)
        roundedRectanglePath.addClip()
        let transform = CGAffineTransformMakeTranslation(-round(roundedRectangleBorderRect.size.width), 0)
        roundedRectangleNegativePath.applyTransform(transform)
        UIColor.grayColor().setFill()
        roundedRectangleNegativePath.fill()
        CGContextRestoreGState(context)
        
        let roundedRectangle2Path = PinInfoView.createBezierPathForSize(boxBounds.size, arrowPosition: arrowPoint)
        
        
        CGContextSaveGState(context)
        roundedRectangle2Path.addClip()
        CGContextRestoreGState(context)
        
        CGContextSaveGState(context)
        roundedRectanglePath.addClip()
        CGContextSetRGBFillColor(context, 0, 0, 0, 0.2)
        CGContextRestoreGState(context)        
        
    }
    
    static func createBezierPathForSize(size: CGSize, arrowPosition: CGPoint) -> UIBezierPath {
        let result = UIBezierPath()
        let width = size.width - shadowPadding * 2
        let height = size.height - shadowPadding * 2
        var startArrowPoint = CGPointMake(-arrowSize.width / 2, -arrowSize.height)
        var endArrowPoint   = CGPointMake(arrowSize.width / 2, -arrowSize.height)
        var topArrowPoint = CGPointZero
        var offset = CGPointMake(shadowPadding, shadowPadding)
        offset = CGPointOffset(offset, dx: arrowPosition.x, dy: height + arrowSize.height)
        let t1 = CGPointZero
        
        startArrowPoint = CGPointOffsetByPoint(startArrowPoint, offsetPoint: offset)
        endArrowPoint = CGPointOffsetByPoint(endArrowPoint, offsetPoint: offset)
        topArrowPoint = CGPointOffsetByPoint(topArrowPoint, offsetPoint: offset)
        
        let createBezierArrow = {
            result.addLineToPoint(startArrowPoint)
            result.addLineToPoint(topArrowPoint)
            result.addLineToPoint(endArrowPoint)
        }
        
        result.moveToPoint(CGPointMake(t1.x + shadowPadding, t1.y + shadowPadding + height - cornoerRadius))
        result.addArcWithCenter(CGPointMake(t1.x + shadowPadding + cornoerRadius, t1.y + shadowPadding + height - cornoerRadius), radius: cornoerRadius, startAngle: radians(180), endAngle: radians(90), clockwise: false)
        createBezierArrow()
        
        result.addLineToPoint(CGPointMake(t1.x + shadowPadding + width - cornoerRadius, t1.y + shadowPadding + height))
        result.addArcWithCenter(CGPointMake(t1.x + shadowPadding + width - cornoerRadius, t1.y + shadowPadding + height - cornoerRadius), radius: cornoerRadius, startAngle: radians(90), endAngle: radians(0), clockwise: false)
        
        result.addLineToPoint(CGPointMake(t1.x + shadowPadding + width, t1.y + shadowPadding + cornoerRadius))
        result.addArcWithCenter(CGPointMake(t1.x + shadowPadding + width - cornoerRadius, t1.y + shadowPadding + cornoerRadius), radius: cornoerRadius, startAngle: radians(0), endAngle: radians(-90), clockwise: false)
        
        
        result.addLineToPoint(CGPointMake(t1.x + shadowPadding + cornoerRadius, t1.y + shadowPadding))
        result.addArcWithCenter(CGPointMake(t1.x + shadowPadding + cornoerRadius, t1.y + shadowPadding + cornoerRadius), radius: cornoerRadius, startAngle: radians(-90), endAngle: radians(-180), clockwise: false)
        
        result.addLineToPoint(CGPointMake(t1.x + shadowPadding, t1.y + shadowPadding + height - cornoerRadius))
        
        result.closePath()
        
        return result
    }
    
}

protocol PinInfoViewDelegate {
    func PinInfoViewDidShow(pinInfoView: PinInfoView)
    func PinInfoViewWillHide(pinInfoView: PinInfoView)
}



