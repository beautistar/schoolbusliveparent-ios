//
//  TripListBodyCell.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 7/29/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit

class TripListBodyCell: UITableViewCell {
    
    @IBOutlet weak var vBodyBg: UIView!
    
    @IBOutlet weak var imvPhoto: UIImageView!

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblCompany: UILabel!
    
    @IBOutlet weak var lblPickUpStatus: UILabel!
    
    @IBOutlet weak var lblDropOffStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        vBodyBg.layer.borderColor = UIColor (colorLiteralRed: 204/255.0, green: 204/255.0, blue: 163/255.0, alpha: 1.0) .CGColor
        
        imvPhoto.layer.borderColor = UIColor (colorLiteralRed: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0) .CGColor
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
