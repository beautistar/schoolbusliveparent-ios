//
//  TripListHeaderCell.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 7/29/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit

class TripListHeaderCell: UITableViewCell {

    @IBOutlet weak var btnToggle: UIButton!
    
    var collapsed : Bool = false   
    
    
    @IBOutlet weak var imvDrop: UIImageView!
    
    @IBOutlet weak var imvStatusBus: UIImageView!
    
    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
