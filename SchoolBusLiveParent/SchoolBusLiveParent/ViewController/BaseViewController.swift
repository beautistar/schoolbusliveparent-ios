//
//  BaseViewController.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 8/1/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit


class BaseViewController: UIViewController {
    
    var HUD: MBProgressHUD!
    
//    var loadingView: LoadingView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    // register keyboard notification
    func registerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // remove keyboard notification
    func deregisrerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillShowNotification)
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillHideNotification)
    }
    
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
    }
    
    // when keyboard is showing, move view up
    func keyboardWillShow(notification: NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        
        let offsetY = keyboardRectangle.height - getOffsetYWhenShowKeyboard()
        
        UIView.animateWithDuration(0.2) { () -> Void in
            
            var f = self.view.frame
            
            f.origin.y = -offsetY;
            
            self.view.frame = f;
        }
    }
    
    // when keyboard is closing, move view down
    func keyboardWillHide(notification: NSNotification) {
        
        UIView.animateWithDuration(0.2) { () -> Void in
            
            var f = self.view.frame
            
            if ((self.navigationController?.navigationBar) != nil) {
                
                f.origin.y = 0
                
            } else {
                
                f.origin.y = 0
            }
            
            self.view.frame = f;
        }
    }
    
    // get offset
    func getOffsetYWhenShowKeyboard() -> CGFloat {
        
        return 0
    }
    
    // show loading view
    
    func showLoadingView() {
        
        showLoadingView(nil, sender:nil)
        
    }
    
    func showColorLoadingView() {
        
        HUD = MBProgressHUD.init(view: self.view)
        
        self.view.addSubview(HUD)
        
        HUD.minSize = CGSizeMake(100, 100)
        HUD.color = UIColor.clearColor()
        HUD.activityIndicatorColor = UIColor(netHex: 0x55AAF0)
        HUD.labelText = nil
        HUD.show(true)
    }
    
    
    
    // show loading view with title
    func showLoadingView(title:String?, sender:AnyObject?) {
        
        HUD = MBProgressHUD.init(view: self.view)
        
        self.view.addSubview(HUD)
        
        HUD.minSize = CGSizeMake(100, 100)
        HUD.color = UIColor.clearColor()
        HUD.activityIndicatorColor = UIColor.whiteColor()
        HUD.delegate = sender as? MBProgressHUDDelegate
        HUD.labelText = title
        
        HUD.show(true)
        
    }
    
    // hide loading view
    func hideLoadingView() {
        HUD.hide(true)
    }
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .Default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .Default, handler: nil))
        }
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

extension String {
    func encodeString() -> String? {
        
        let customAllowedSet =  NSCharacterSet(charactersInString:"!*'();:@&=+$,/?%#[] ").invertedSet
        return stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
    }
}