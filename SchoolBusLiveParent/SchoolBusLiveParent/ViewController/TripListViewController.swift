//
//  TripListViewController.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 7/29/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension UIView {
    func rotate(toValue: CGFloat, duration: CFTimeInterval = 0.2, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = toValue
        rotateAnimation.duration = duration
        rotateAnimation.removedOnCompletion = false
        rotateAnimation.fillMode = kCAFillModeForwards
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        self.layer.addAnimation(rotateAnimation, forKey: nil)
    }
}

class TripListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {


    var _customer : CustomerEntity?
    
    var _tripsList: [TripEntity] = []
    
//    var selectedIndexPath: NSIndexPath!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var weeks : [String] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    
    var firstLoad:Int = 1;
    
    var selectedTrip: TripEntity?

    
    @IBOutlet weak var tblTripList: UITableView!
    //
    // MARK: - Data
    //
    struct Section {
        var name: String!
        var items: [TripEntity]!
        var collapsed: Bool!
        
        init(name: String, items: [TripEntity], collapsed: Bool = false) {
            self.name = name
            self.items = items
            self.collapsed = collapsed
        }
    }
    
    var sections = [Section]()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        firstLoad = 1;
        
        _customer = appDelegate._customer
        
        getTripInfo()
        
    }
    
    override func viewWillAppear(animated: Bool) {
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    
    
    //
    //MARK: - get trip list
    //
    
    func getTripInfo() {
        
        showColorLoadingView()

        let url = Req.BASE_URL + Req.TRIPLIST + "&" + Req.CGUID + "=DEMOGUIDCUST" + "&" + Req.TGUID + "=all"
        //print(url)
        
        let escapedURL = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        print(escapedURL)
        
        Alamofire.request(.GET, escapedURL!).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                self.hideLoadingView()
                print(result)
                
/*
                
                if let error = result["error"] as? String {
                    
                    self.showAlertDialog(nil, message: error, positive: "OK", negative: nil)
                    
                } else if let info = result["info"] as? String {
                    
                    self.showAlertDialog(nil, message: info, positive: "OK", negative: nil)
                    
                } else {
                    
                    self._tripsList.removeAllObjects()
                    
                    let trips = result["Trips"] as! Array<AnyObject>
                    
                    for item in trips{
                        
                        let tripEntity = TripEntity()
                        
                        let item_obj = item as! [String : AnyObject]
                        
                        tripEntity._tGuid = item_obj["TGUID"] as! String
                        print (tripEntity._tGuid)
                    }

                }
                
*/
                let jsonResult = JSON(result)
                
                if let error = jsonResult["error"].string {
                    
                    self.showAlertDialog(nil, message: error, positive: "OK", negative: nil)
                    
                } else if let info = jsonResult["info"].string {

                    self.showAlertDialog(nil, message: info, positive: "OK", negative: nil)
                    
                } else {

                    self._tripsList.removeAll()
                    
                    for index in 0 ..< jsonResult["Trips"].array!.count {
                    
                        let tripEntity = TripEntity()
                        
                        tripEntity._id = index
                        tripEntity._tGuid = jsonResult["Trips"][index]["TGUID"].string!
                        tripEntity._pGuid = jsonResult["Trips"][index]["PGUID"].string!
                        tripEntity._tStatusId = jsonResult["Trips"][index]["TStatusID"].int
                        tripEntity._company = jsonResult["Trips"][index]["Company"].string!
                        tripEntity._tDateStarted = jsonResult["Trips"][index]["TDateStarted"].string!
                        tripEntity._tDateEnded = jsonResult["Trips"][index]["TDateEnded"].string!
                        
                        // it will store only day. example "7/27/2016"
                        var startStrArr = jsonResult["Trips"][index]["TDateStarted"].string!.characters.split{$0 == " "}.map(String.init)
                        tripEntity._startDate = startStrArr[0]
                       
                        let pickup = LocationEntity()
                        
                        pickup._title = jsonResult["Trips"][index]["PickUp"]["Title"].string!
                        pickup._addrTypeId = jsonResult["Trips"][index]["PickUp"]["AddrTypeID"].int!
                        pickup._addrLine1 = jsonResult["Trips"][index]["PickUp"]["AddrLine1"].string!
                        pickup._addrLine2 = jsonResult["Trips"][index]["PickUp"]["AddrLine2"].string!
                        pickup._lat = Double(jsonResult["Trips"][index]["PickUp"]["Lat"].string!)
                        pickup._lon = Double(jsonResult["Trips"][index]["PickUp"]["Lng"].string!)
                        pickup._dataOccurred = jsonResult["Trips"][index]["PickUp"]["DateOccurred"].string!
                        pickup.setCoordinate(pickup._lat, lon: pickup._lon)
                        tripEntity._pickUp = pickup
                        
                        let drpOff = LocationEntity()
                        
                        drpOff._title = jsonResult["Trips"][index]["DropOff"]["Title"].string!
                        drpOff._addrTypeId = jsonResult["Trips"][index]["PickUp"]["AddrTypeID"].int!
                        drpOff._addrLine1 = jsonResult["Trips"][index]["PickUp"]["AddrLine1"].string!
                        drpOff._addrLine2 = jsonResult["Trips"][index]["PickUp"]["AddrLine2"].string!
                        drpOff._lat = Double(jsonResult["Trips"][index]["PickUp"]["Lat"].string!)
                        drpOff._lon = Double(jsonResult["Trips"][index]["PickUp"]["Lng"].string!)
                        drpOff._dataOccurred = jsonResult["Trips"][index]["PickUp"]["DateOccurred"].string!
                        drpOff.setCoordinate(drpOff._lat, lon: drpOff._lon)
                        tripEntity._dropOff = drpOff
                        
                        self._tripsList.append(tripEntity)
                        
                    }
                    
                    self.setTripList()
                    
                    //print(self._tripsList.count)
                }
                break
            case .Failure(let error):
                
                print(error)
                self.hideLoadingView()
                self.showAlertDialog(nil, message: "Connection to the server failed. \n Please try again.", positive: "OK", negative: nil)
                break
            }
        })
    }
    
    func setTripList() {
        
        self.sections.removeAll()

        // sort by tDateStarted decresing
        _tripsList = _tripsList.sort({ $0._tDateStarted.compare($1._tDateStarted) == .OrderedDescending })
        
        var startDate = _tripsList[0]._startDate
        
        var items: [TripEntity] = []
        items.append(_tripsList[0])
        
        for index in 1 ..< _tripsList.count {
            
            if (startDate == _tripsList[index]._startDate) {
            
                items.append(_tripsList[index])
                
            } else {
                
                sections.append(Section(name: startDate, items: items))
                
                startDate = _tripsList[index]._startDate
                
                items.removeAll()
                items.append(_tripsList[index])
            }
        
        
            if (index == _tripsList.count - 1) {
            
                sections.append(Section(name: startDate, items: items))
            }
        }
        
        self.tblTripList.reloadData()

    }

    
    //
    //MARK: - UITableViewDelegate
    //
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return sections.count
    }
    
     func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 && firstLoad == 1{
            
            firstLoad = 0

            let collapsed = sections[section].collapsed
            sections[section].collapsed = !collapsed
            
            return sections[section].items.count
            
        } else {
            
            return (!sections[section].collapsed!) ? 0 : sections[section].items.count
        }
    }
    
     func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCellWithIdentifier("TripListHeaderCell") as! TripListHeaderCell
        
        header.btnToggle.tag = section;
        
        // change the header date string to string format : "Jul 27 - Saturday"
        
        if section == 0 {
            
            header.imvStatusBus.image = UIImage(named: "sbl_trip_75_live")
            
            header.lblDate.text = getMonthStr(sections[section].name!) + "-" + weeks[getDayOfWeek(sections[section].name!) - 1] + " (Today)"
            
        } else {
            
            header.imvStatusBus.image = UIImage(named: "sbl_trip_75_done")
            header.lblDate.text = getMonthStr(sections[section].name!) + " - " + weeks[getDayOfWeek(sections[section].name!) - 1]
        }
        
        header.imvDrop.rotate(!sections[section].collapsed! ? 0.0 : CGFloat(M_PI_2 * 2))
        header.btnToggle.addTarget(self, action: #selector(TripListViewController.toggleCollapse), forControlEvents: .TouchUpInside)
        
        return header.contentView
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TripListBodyCell", forIndexPath:  indexPath) as! TripListBodyCell
        
        let trip = sections[indexPath.section].items[indexPath.row]
        
        cell.lblName.text = _customer?._fullname
        cell.lblCompany.text = trip._company
        
//        let imgURL: NSURL = NSURL(string: _customer.)!
//        let request: NSURLRequest = NSURLRequest(URL: imgURL)
//        
//        let session = NSURLSession.sharedSession()
//        let task = session.dataTaskWithRequest(request){
//            (data, response, error) -> Void in
//            
//            if (error == nil && data != nil) {
//                func display_image() {
//                    cell.imvPhoto.image = UIImage(data: data!)
//                }
//                
//                dispatch_async(dispatch_get_main_queue(), display_image)
//            }
//        }
//        
//        task.resume()
        
        

//        
//        print(trip._tStatusId)
//        print(trip._tDateStarted)
        
        if (trip._tStatusId == 111) { // Expecting
            
            let textColor = UIColor (red: 80/255.0, green: 137/255.0, blue: 12/255.0, alpha: 1.0)
            let bgColor = UIColor (red: 241/255.0, green: 251/255.0, blue: 224/255.0, alpha: 1.0)

            
            cell.lblName.textColor = UIColor .whiteColor()
            cell.lblName.backgroundColor = UIColor (red: 108/255.0, green: 190/255.0, blue: 12/255.0, alpha: 1.0)
            
            cell.lblPickUpStatus.textColor = textColor
            cell.lblPickUpStatus.backgroundColor = bgColor
            
            cell.lblDropOffStatus.textColor = textColor
            cell.lblDropOffStatus.backgroundColor = bgColor
            
            // set text
            cell.lblPickUpStatus.text = "Expecting..."
            cell.lblDropOffStatus.text = "..."
            
        } else if (trip._tStatusId == 222) {// OK
            
            let textColor = UIColor (red: 12/255.0, green: 105/255.0, blue: 137/255.0, alpha: 1.0)
            
            cell.lblName.textColor = textColor
            cell.lblName.backgroundColor = UIColor (red: 190/225.0, green: 246/225.0, blue: 251/255.0, alpha: 1.0)
            
            
            cell.lblPickUpStatus.textColor = textColor
            cell.lblDropOffStatus.textColor = textColor
            
            let pickupTimeArr = trip._tDateStarted.characters.split{$0 == " "}.map(String.init)
            let dropoffTimeArr = trip._tDateEnded.characters.split{$0 == " "}.map(String.init)
            
            cell.lblPickUpStatus.text = getConvertTime(pickupTimeArr[1]) + " (OK)"
            cell.lblDropOffStatus.text = getConvertTime(dropoffTimeArr[1]) + " (OK)"
            
        } else { // Absent
            
            let textColor = UIColor (red: 137/255.0, green: 23/255.0, blue: 12/255.0, alpha: 1.0)
            
            cell.lblName.textColor = textColor
            cell.lblName.backgroundColor = UIColor (red: 254/225.0, green: 198/225.0, blue: 198/255.0, alpha: 1.0)
            
            cell.lblPickUpStatus.textColor = textColor
            cell.lblDropOffStatus.textColor = textColor
            
            let pickupTimeArr = trip._tDateStarted.characters.split{$0 == " "}.map(String.init)
            
            cell.lblPickUpStatus.text = getConvertTime(pickupTimeArr[1]) + " (ABSENT)"
            cell.lblDropOffStatus.text = "SUSPENDED"
            
        }
        
        return cell
    }
    
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedTrip = sections[indexPath.section].items[indexPath.row]
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        //testing
        
        let selectedIndexPath = tblTripList.indexPathForSelectedRow

        let triponVC = segue.destinationViewController as! TripOnMapViewController
        triponVC._trip = sections[selectedIndexPath!.section].items[selectedIndexPath!.row]

    }

    
    //
    // MARK: - Event Handlers
    //
    func toggleCollapse(sender: UIButton) {
        
        let section = sender.tag
        
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed
        
        // Reload section
        tblTripList.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
    }
    
    //
    // MARK: - DateTime formatters
    //
    
    
    // get week day from "7/26/2016". return value = int, 1: Sunday, 7:Saturday
    
    func getDayOfWeek(today:String!)->Int {
        
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let todayDate = formatter.dateFromString(today)
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let myComponents = myCalendar.components(.Weekday, fromDate: todayDate!)
        let weekDay = myComponents.weekday
        return weekDay
    }
    
    
    //convert format "7/26/2016" to "Jul 26"
    
    func getMonthStr(dateString:String!)->String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = ("M/dd/yyyy")     // origin date format string
        //dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let dateObj = dateFormatter.dateFromString(dateString)
        
        dateFormatter.dateFormat = ("MMM dd")
        //dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let desDate = dateFormatter.stringFromDate(dateObj!)
        
        return desDate
    }
    
    // convert from time format "14:12:00" to "2:12am"
    func getConvertTime(timeString:String!) ->String {
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let time_ = formatter.dateFromString(timeString)
        
        formatter.dateFormat = "HH:mm a"
        let time = formatter.stringFromDate(time_!)
        
        return time
        
    }
    
    // load children image with url
//    func load_driverimage(urlString:String) {
//        let imgURL: NSURL = NSURL(string: urlString)!
//        let request: NSURLRequest = NSURLRequest(URL: imgURL)
//        
//        let session = NSURLSession.sharedSession()
//        let task = session.dataTaskWithRequest(request){
//            (data, response, error) -> Void in
//            
//            if (error == nil && data != nil) {
//                func display_image() {
//                    s.image = UIImage(data: data!)
//                }
//                
//                dispatch_async(dispatch_get_main_queue(), display_image)
//            }
//        }
//        
//        task.resume()
//    }
}
