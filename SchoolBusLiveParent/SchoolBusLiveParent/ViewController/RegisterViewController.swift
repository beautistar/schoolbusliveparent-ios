//
//  RegisterViewController.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 7/29/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import SwiftyJSON

class RegisterViewController: BaseViewController, UITextFieldDelegate {
    
    var _customer:CustomerEntity?

    @IBOutlet weak var txfFirstName: UITextField!
    
    @IBOutlet weak var txfLastName: UITextField!
    
    @IBOutlet weak var txfGender: UITextField!    
    
    @IBOutlet weak var txfEmail: UITextField!
    
    @IBOutlet weak var txfPassword: UITextField!
    
    @IBOutlet weak var txfRePassword: UITextField!
    
    @IBOutlet weak var btnGender: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initVars()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        registerForKeyboardNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        deregisrerForKeyboardNotifications()
    }
    
    func initVars() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        _customer = appDelegate._customer
    }
    
    
    @IBAction func cancelAction(sender: UIButton) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    


    @IBAction func registerAction(sender: AnyObject?) {
        
        if isValid() {
            
            doRegister()
        }
        
    }
    
    func isValid() ->Bool {
        
        if (txfFirstName.text?.isEmpty == true) {
            
            showAlertDialog(nil, message: "Please input first name", positive: "OK", negative: nil)
            return false
            
        } else if (txfLastName.text?.isEmpty == true) {
            
            showAlertDialog(nil, message: "Please input last name", positive: "OK", negative: nil)
            return false
            
        } else if (txfGender.text?.isEmpty == true) {
            
            showAlertDialog(nil, message: "Please select gender", positive: "OK", negative: nil)
            return false
            
        } else if (txfEmail.text?.isEmpty == true) {
            
            showAlertDialog(nil, message: "Please input Email Address", positive: "OK", negative: nil)
            
            return false
            
        } else if !isValidEmail(txfEmail.text!) {
            
            showAlertDialog(nil, message: "Please enter the correct email address", positive: "OK", negative: nil)
            
            return false
            
        } else if (txfPassword.text?.isEmpty == true) {
            
            showAlertDialog(nil, message: "Please input password", positive: "OK", negative: nil)
            
            return false
            
        } else if (txfPassword.text != txfRePassword.text) {
            
            showAlertDialog(nil, message: "Please input correct password", positive: "OK", negative: nil)
            
            return false
            
        }
        
        return true
    }
    
    func doRegister() {
        
        showColorLoadingView()
        //showLoadingView()
        
        let url = Req.BASE_URL + Req.REGISTER +
            "&" + Req.FIRSTNAME + "=\(txfFirstName.text!)" +
            "&" + Req.LASTNAME + "\(txfLastName.text!)" +
            "&" + Req.GENDER + "\(txfGender.text!)" +
            "&" + Req.EMAIL + "\(txfEmail.text!)" +
            "&" + Req.PWD + "\(txfPassword.text!)"
        
        print(url)
        
        let escapedURL = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        
        Alamofire.request(.GET, escapedURL!).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                self.hideLoadingView()
                print(result)
                
                
                
                let jsonResult = JSON(result)
                
                if let _ = jsonResult["error"].string {
                    
                    let error = jsonResult["error"].string!
                    
                    self.hideLoadingView()
                    self.showAlertDialog(nil, message: error, positive: "OK", negative: nil)
                    
                } else {
                    
                    self._customer?._guid = jsonResult["Customer"]["guid"].string!
                    self._customer?._status = jsonResult["Customer"]["guid"].int
                    self._customer?._fullname = jsonResult["Customer"]["fullname"].string!
                    self._customer?._email = jsonResult["Customer"]["email"].string!
                    self._customer?._phone = jsonResult["Customer"]["phone"].string!
                    
                    self._customer?.saveCustomerInfo();
                
                    
                    self.gotoTripListVC()
                    
                    
                    //                let passengers = jsonResult["passenger"].array
                    //
                    //                for index in 0 ..< passengers?.count {
                    //
                    //                    jsonResult["Passesngers"][index]["guid"].string!
                    //
                    //                }
                    
                }
                
            case .Failure(let error):
                
                print(error)
                self.hideLoadingView()
                self.showAlertDialog(nil, message: "Connection to the server failed. \n Please try again.", positive: "OK", negative: nil)
                
                
            }
        })
        
    }
    
    func gotoTripListVC() {
        
        let tripListVC = self.storyboard?.instantiateViewControllerWithIdentifier("TripListViewController") as? TripListViewController
        
        self.navigationController?.pushViewController(tripListVC!, animated: true)
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
    
    // register keyboard notification
    override func registerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // remove keyboard notification
    override func deregisrerForKeyboardNotifications() {
        
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillShowNotification)
        NSNotificationCenter.defaultCenter().removeObserver(UIKeyboardWillHideNotification)
    }
    
    @IBAction func genderSelectAction(sender: AnyObject) {
        
        self.view .endEditing(true)
        
        ActionSheetStringPicker.showPickerWithTitle("Select your gender", rows: ["Male", "Female"], initialSelection: 0, doneBlock: {(picker, index, value) in
            
            self.txfGender.text = value as! String!
            
            }, cancelBlock: {ActionStringCancelBlock in return }, origin: sender)
    }
    
    override func getOffsetYWhenShowKeyboard() -> CGFloat {
        
        
        if (txfEmail.isFirstResponder()) {
            
            return 200
            
        } else if (txfPassword.isFirstResponder()) {
            
            return 150
        } else if (txfRePassword.isFirstResponder()) {
            
            return 100
        }
        
        return 220;
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }

}
