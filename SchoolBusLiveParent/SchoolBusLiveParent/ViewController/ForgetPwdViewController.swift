//
//  ForgetPwdViewController.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 7/29/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JLToast

class ForgetPwdViewController: BaseViewController {

    @IBOutlet weak var txfEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func forgetPwdAction(sender: AnyObject) {
        
        if (isValid()) {
            
            doForgetPwd()
        }
        
        
    }
    
    func  isValid() -> Bool {
        
        if (txfEmail.text?.isEmpty == true) {
            
            
            showAlertDialog(nil, message: "Please input Email Address", positive: "OK", negative: nil)
            
            return false
            
        } else if !isValidEmail(txfEmail.text!) {
            
            showAlertDialog(nil, message: "Please enter the correct email address.", positive: "OK", negative: nil)
            
            return false
        }
        
        return true
    }
    
    func doForgetPwd() {
        
        showColorLoadingView()
        //showLoadingView(nil, sender: self)
        
        let url = Req.BASE_URL + Req.FORGOT + "&" + Req.EMAIL + "=\(txfEmail.text!)"
        print(url)
        
        let escapedURL = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        
        Alamofire.request(.GET, escapedURL!).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                self.hideLoadingView()
                print(result)               
                
                
                let jsonResult = JSON(result)
                
                if let _ = jsonResult["error"].string {
                    
                    let error = jsonResult["error"].string!
                    
                    self.showAlertDialog(nil, message: error, positive: "OK", negative: nil)
                    
                } else {
                    
                    let done = jsonResult["done"].string!
                    
                    JLToast.makeText(done).show()
                    
                }
                
            case .Failure(let error):
                
                print(error)
                self.hideLoadingView()
                self.showAlertDialog(nil, message: "Connection to the server failed. \n Please try again.", positive: "OK", negative: nil)
                
            }
        })

        
        
    }
    


}
