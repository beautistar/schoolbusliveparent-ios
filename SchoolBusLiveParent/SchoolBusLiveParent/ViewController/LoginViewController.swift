//
//  LoginViewController.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 7/29/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

var Passengers: [PassengerEntity] = []

class LoginViewController: BaseViewController, UITextFieldDelegate {
    
    var _cosutomer: CustomerEntity?
    
    

    @IBOutlet weak var txfEmail: UITextField!
    @IBOutlet weak var txfPassword: UITextField!
    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        _cosutomer = appDelegate._customer
        
//        _cosutomer = CustomerEntity.loadCustomerInfo()
        
//        let defaults = NSUserDefaults.standardUserDefaults()
//        
//        _cosutomer?._isLoggedIn = defaults.integerForKey("Is_LoggedIn")
//        _cosutomer?._guid = defaults.objectForKey("Customer_Guid") as! String?
        
        print(_cosutomer?._isLoggedIn)
        print(_cosutomer?._guid)
        

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBarHidden = true
    }

    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        registerForKeyboardNotifications()
        
        if _cosutomer!._isLoggedIn == 1 {
            
            gotoTripListVC()
        } 
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        deregisrerForKeyboardNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func loginAction(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if (isValid()) {
            
            doLogin()
        }
    }
    
    func isValid() ->Bool {
        
        if (txfEmail.text?.isEmpty == true) {
            
            showAlertDialog(nil, message: "Please input Email Address", positive: "OK", negative: nil)
            
            return false
        
        } else if !isValidEmail(txfEmail.text!) {
           
            showAlertDialog(nil, message: "Please enter the correct email address.", positive: "OK", negative: nil)
            
            return false
            
        } else if (txfPassword.text?.isEmpty == true) {
            showAlertDialog(nil, message: "Please input Password", positive: "OK", negative: nil)
            return false
        }
        
        return true
    }
    
    func doLogin() {
        
        //showLoadingView(nil, sender: self)
        showColorLoadingView()
        
        let url = Req.BASE_URL + Req.LOGIN + "&" + Req.EMAIL + "=\(txfEmail.text!)" + "&" + Req.PWD + "=\(txfPassword.text!)"
        
        print(url)

        let escapedURL = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        
        Alamofire.request(.GET, escapedURL!).responseJSON(completionHandler: { response in
            
        switch response.result {
            
            case .Success(let result):
                
                self.hideLoadingView()
                print(result)
                
                let jsonResult = JSON(result)
                
                if let error = jsonResult["error"].string {
                    
                    //let error = jsonResult["error"].string!
                    
                    self.hideLoadingView()
                    self.showAlertDialog(nil, message: error, positive: "OK", negative: nil)
                    
                } else {
                    
                    self._cosutomer?._guid = jsonResult["Customer"]["guid"].string!
                    self._cosutomer?._status = jsonResult["Customer"]["status"].int
                    self._cosutomer?._fullname = jsonResult["Customer"]["fullname"].string!
                    self._cosutomer?._email = jsonResult["Customer"]["email"].string!
                    self._cosutomer?._phone = jsonResult["Customer"]["phone"].string!
                    self._cosutomer?._isLoggedIn = 1
                    
//                    print(jsonResult["Customer"]["phone"].string!)
                    self._cosutomer?.saveCustomerInfo();
                    



                
//                    for _index in 0 ..< jsonResult["Passengers"].array!.count {
//                        
//                        let passengerEntity = PassengerEntity()
//                   
//                        passengerEntity._guid = jsonResult["Passesngers"][_index]["guid"].string!
//                        passengerEntity._status = jsonResult["Passengers"][_index]["status"].int
//                        passengerEntity._fullName = jsonResult["Passengers"][_index]["fullname"].string!
//                        passengerEntity._gender = jsonResult["Passengers"][_index]["gender"].string!
//                        passengerEntity._dob = jsonResult["Passenger"][_index]["dob"].string!
//                        passengerEntity._phone = jsonResult["Passenger"][_index]["phone"].string!
//                        passengerEntity._picurl = jsonResult["Passenger"][_index]["picurl"].string!
//                        
//                        Passengers.append(passengerEntity)
//                        
//                    }
                    
                    
                    self.gotoTripListVC()
            
            }
        
        case .Failure(let error):
                
                print(error)
                self.hideLoadingView()
                self.showAlertDialog(nil, message: "Connection to the server failed. \n Please try again.", positive: "OK", negative: nil)
            
            }
        })
    
    }

    func gotoTripListVC() {
        
        let tripListVC = self.storyboard?.instantiateViewControllerWithIdentifier("TripListViewController") as? TripListViewController
        
        
        self.navigationController?.pushViewController(tripListVC!, animated: true)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view .endEditing(true)
        
        
    }
    
    override func getOffsetYWhenShowKeyboard() -> CGFloat {
      
        if (txfPassword.isFirstResponder()) {
            
            return 150
            
        }
   
        return 200
    }
    
}
