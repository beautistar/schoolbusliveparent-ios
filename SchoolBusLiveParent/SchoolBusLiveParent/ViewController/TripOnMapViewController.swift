//
//  TripOnMapViewController.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 7/30/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import MapKit


class TripOnMapViewController: BaseViewController, GMSMapViewDelegate, MKMapViewDelegate, PinInfoViewDelegate {

    @IBOutlet weak var vForMap: UIView!
    
    @IBOutlet weak var vDetailBg: UIView!
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var lblWelcomename: UILabel!
    
    @IBOutlet weak var lblChildname: UILabel!
    @IBOutlet weak var lblPickUp: UILabel!
    @IBOutlet weak var lblDropOff: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.menuDropDown
        ]
    }()
    
    @IBOutlet weak var mapviewMain: MKMapView!

    
    var mapView : GMSMapView!
    
    var currentlyTappedMarker: GMSMarker!
    
    var _trip = TripEntity()
    
    var _tripsList: [TripEntity] = []
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var _customer : CustomerEntity?
    
    var markerTapped:Bool!
    var cameraMoving:Bool!
    var idleAfterMovement:Bool!
    
    var timer = NSTimer()
    
    var tCnt : Int = 0
    
    let menuDropDown = DropDown()
    
    var _menuItems: NSArray!
    
    var satMapType:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        markerTapped = false
        cameraMoving = false
        idleAfterMovement = false
        
        _customer = appDelegate._customer

        setupDropDown()
        
    }
    
    
    //MARK: - Setup
    
    func setupDropDown() {
    
        menuDropDown.anchorView = btnMenu
        
        menuDropDown.bottomOffset = CGPoint(x: 0, y: btnMenu.bounds.height)

        menuDropDown.dataSource = [
            "My Account",
            "My Settings",
            "Logout",
            "About Us",
            "Exit SchoolBusLive"
        ]
        
        // Action triggered on selection
        menuDropDown.selectionAction = {  (index, item) in
            
            print("\(index) is selected")
            
            switch index {
                
            case 0:
                
                self.gotoAboutUs()
                
                break
                
            default:
                
                let aboutUsVC = self.storyboard?.instantiateViewControllerWithIdentifier("AboutUsViewController") as? AboutUsViewController
                
                self.navigationController?.pushViewController(aboutUsVC!, animated: true)
                
                break

            }
        }        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(true)
        
        tCnt = 1
        
        initView()
        
        if (_trip._tStatusId == 111) {
            
            self.getLastLocation()
            timer.invalidate()

            timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(getLastLocation), userInfo: nil, repeats: true)

        } else {
            
            initMKMapView()
            
        }
        
    }
    
    func initView() {
        
        vDetailBg.layer.borderColor = UIColor (colorLiteralRed: 204/255.0, green: 204/255.0, blue: 163/255.0, alpha: 1.0) .CGColor
        
        imvPhoto.layer.borderColor = UIColor (colorLiteralRed: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0) .CGColor
        

    }
    
    //MARK:- DownMenu
    @IBAction func btnMenuAction(sender: AnyObject) {
        
        menuDropDown.show()
    }
    
    func gotoAboutUs()  {
        
        let aboutUsVC = self.storyboard?.instantiateViewControllerWithIdentifier("AboutUsViewController") as? AboutUsViewController
        
        self.navigationController?.pushViewController(aboutUsVC!, animated: true)
        
        
    }
    
    func getLastLocation() {
        
        tCnt += 20  // temp, for test
        
        if self.tCnt == 594 {
            
            tCnt = 1
        }
        
        //let url = Req.BASE_URL + Req.TRIPLIST + "&" + Req.CGUID + "=DEMOGUIDCUST" + "&" + Req.TGUID + "=today"
        let url = Req.BASE_URL + Req.TRIPLIST + "&" + Req.CGUID + "=DEMOGUIDCUST" + "&" + Req.TGUID + "=2016TRIP05&loc=" + "\(tCnt)"
        
        
        let escapedURL = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        print(escapedURL)
        
        Alamofire.request(.GET, escapedURL!).responseJSON(completionHandler: { response in
            
            switch response.result {
                
            case .Success(let result):
                
                print(result)
                
                let jsonResult = JSON(result)
                
                if let error = jsonResult["error"].string {
                    
                    self.showAlertDialog(nil, message: error, positive: "OK", negative: nil)
                    
                } else if let info = jsonResult["info"].string {
                    
                    self.showAlertDialog(nil, message: info, positive: "OK", negative: nil)
                    
                } else {
                    
                    for index in 0 ..< jsonResult["Trips"].array!.count {
                        
                       
                       //_trip._id = index
                        self._trip._tGuid = jsonResult["Trips"][index]["TGUID"].string!
                        self._trip._pGuid = jsonResult["Trips"][index]["PGUID"].string!
                        self._trip._tStatusId = jsonResult["Trips"][index]["TStatusID"].int!
                        self._trip._company = jsonResult["Trips"][index]["Company"].string!
                        self._trip._tDateStarted = jsonResult["Trips"][index]["TDateStarted"].string!
                        self._trip._tDateEnded = jsonResult["Trips"][index]["TDateEnded"].string!
                        
                        // it will store only day. example "7/27/2016"
                        var startStrArr = jsonResult["Trips"][index]["TDateStarted"].string!.characters.split{$0 == " "}.map(String.init)
                        self._trip._startDate = startStrArr[0]
                        
                        let pickup = LocationEntity()
                        
                        pickup._title = jsonResult["Trips"][index]["PickUp"]["Title"].string!
                        pickup._addrTypeId = jsonResult["Trips"][index]["PickUp"]["AddrTypeID"].int!
                        pickup._addrLine1 = jsonResult["Trips"][index]["PickUp"]["AddrLine1"].string!
                        pickup._addrLine2 = jsonResult["Trips"][index]["PickUp"]["AddrLine2"].string!
                        pickup._lat = Double(jsonResult["Trips"][index]["PickUp"]["Lat"].string!)
                        pickup._lon = Double(jsonResult["Trips"][index]["PickUp"]["Lng"].string!)
                        pickup._dataOccurred = jsonResult["Trips"][index]["PickUp"]["DateOccurred"].string!
                        pickup.setCoordinate(pickup._lat, lon: pickup._lon)
                        self._trip._pickUp = pickup
                        
                        let drpOff = LocationEntity()
                        
                        drpOff._title = jsonResult["Trips"][index]["DropOff"]["Title"].string!
                        drpOff._addrTypeId = jsonResult["Trips"][index]["PickUp"]["AddrTypeID"].int!
                        drpOff._addrLine1 = jsonResult["Trips"][index]["PickUp"]["AddrLine1"].string!
                        drpOff._addrLine2 = jsonResult["Trips"][index]["PickUp"]["AddrLine2"].string!
                        drpOff._lat = Double(jsonResult["Trips"][index]["PickUp"]["Lat"].string!)! + 0.02  // temp,  for test
                        drpOff._lon = Double(jsonResult["Trips"][index]["PickUp"]["Lng"].string!)! - 0.02  // temp
                        drpOff._dataOccurred = jsonResult["Trips"][index]["PickUp"]["DateOccurred"].string!
                        drpOff.setCoordinate(drpOff._lat, lon: drpOff._lon)
                        self._trip._dropOff = drpOff
                        
                        let lastLocation = LLocationEntity()
                        
                        lastLocation._bearing = Int(jsonResult["Trips"][index]["LastLocation"]["Bearing"].string!)
                        lastLocation._lat = Double(jsonResult["Trips"][index]["LastLocation"]["Lat"].string!)
                        lastLocation._lng = Double(jsonResult["Trips"][index]["LastLocation"]["Lng"].string!)
                        lastLocation._dateReported = jsonResult["Trips"][index]["LastLocation"]["DateReported"].string!

                        lastLocation.setCoordinate(lastLocation._lat, lon: lastLocation._lng)
                        
                        self._trip._lastLocation = lastLocation
                        
                    }
                    
                    self.initMKMapView()

                }
                break
            case .Failure(let error):
                
                print(error)
                //self.hideLoadingView()
                self.showAlertDialog(nil, message: "Connection to the server failed. \n Please try again.", positive: "OK", negative: nil)
                break
            }
        })
        
    }
    
    @IBAction func btnPickupAction(sender: AnyObject) {
        
        self.mapviewMain.setCenterCoordinate(_trip._pickUp.coordinate, animated: true)
        
    }
    
    
    @IBAction func btnDropOffAction(sender: AnyObject) {
        
        self.mapviewMain.setCenterCoordinate(_trip._dropOff.coordinate, animated: true)
        
    }
    
    @IBAction func btnSatAction(sender: AnyObject) {
        
        if satMapType {
            
            mapviewMain.mapType = MKMapType.Standard
            
        } else {
            
            mapviewMain.mapType = MKMapType.Satellite
        }
        
        satMapType = !satMapType
    }
    
    //MARK : -MKMapView delegate
    
    private func initMKMapView() ->Void {
        
        
        if (_trip._tStatusId == 111) {
            
            let annotations = mapviewMain.annotations
            self.mapviewMain.removeAnnotations(annotations)
            
            self.mapviewMain.addAnnotations([_trip._pickUp, _trip._dropOff, _trip._lastLocation])

            self.mapviewMain.showAnnotations([_trip._pickUp, _trip._dropOff, _trip._lastLocation], animated: false)
            
        } else {
            
            self.mapviewMain.addAnnotations([_trip._pickUp, _trip._dropOff])
            self.mapviewMain.centerCoordinate = _trip._pickUp.coordinate
            self.mapviewMain.showAnnotations([_trip._pickUp, _trip._dropOff, ], animated: true)
        }
        
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        
        if view.annotation!.isKindOfClass(LocationEntity) {
            
            let object = view.annotation as! LocationEntity
            let pinFrame = mapMoveAnimationWithMKAnnotationView(view)
            PinInfoView.showInView(self.mapviewMain, pinFrame: pinFrame, delegate: self, object: object)
        }
    }
    
    func mapView(mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        
        PinInfoView.hideFromView(self.mapviewMain, animate: true)
        deselectAllAnnotation()
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation as? MKUserLocation == mapView.userLocation {
            return nil
        }
        
        let pinReuseIdentifier = "pinForLocationEntity"
        var pinView: MKAnnotationView? = mapView.dequeueReusableAnnotationViewWithIdentifier(pinReuseIdentifier)
        if pinView == nil {

            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: pinReuseIdentifier)
            pinView!.frame = CGRectMake(0, 0, 48, 48)
            pinView?.centerOffset = CGPointMake(0, -40)
            let pinIconView = UIImageView(image: UIImage(named: "sbl_mrk_home_green"))
            pinView!.addSubview(pinIconView)
            pinIconView.tag = 102
        }
        
        pinView?.annotation = annotation
        
        let pinIconView = pinView?.viewWithTag(102) as? UIImageView
        
        var pickupMrkImg : String!
        var dropOffMrkImg : String!
        var busMrkImg : String!
        
        if (_trip._tStatusId == 111) {
            
            pickupMrkImg = "sbl_mrk_home_green"
            dropOffMrkImg = "sbl_mrk_school_green"
            busMrkImg = "sbl_map_me_48_on"
            
        } else if (_trip._tStatusId == 222) {
            
            pickupMrkImg = "sbl_mrk_home_blue"
            dropOffMrkImg = "sbl_mrk_school_blue"
            
        } else {
            
            pickupMrkImg = "sbl_mrk_home_red"
            dropOffMrkImg = "sbl_mrk_school_red"
        }
        
        if annotation as? LocationEntity == _trip._pickUp {
            pinIconView?.image = UIImage(named: pickupMrkImg)
        } else if annotation as? LocationEntity == _trip._dropOff {
            pinIconView?.image = UIImage(named: dropOffMrkImg)
        } else if annotation as? LLocationEntity == _trip._lastLocation {
            pinIconView?.image = UIImage(named: busMrkImg)
        }
        
        return pinView
    }
    
    func configureDetailView(annotationView: MKAnnotationView) {
        let width = 290
        let height = 120
        
        let snapshotView = UIView()
        let views = ["snapshotView": snapshotView]
        snapshotView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[snapshotView(290)]", options: [], metrics: nil, views: views))
        snapshotView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[snapshotView(120)]", options: [], metrics: nil, views: views))
        
        let options = MKMapSnapshotOptions()
        options.size = CGSize(width: width, height: height)
        if #available(iOS 9.0, *) {
            options.mapType = .SatelliteFlyover
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 9.0, *) {
            options.camera = MKMapCamera(lookingAtCenterCoordinate: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)
        } else {
            // Fallback on earlier versions
        }
        
        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.startWithCompletionHandler { snapshot, error in
            if snapshot != nil {
                let customView = NSBundle.mainBundle().loadNibNamed("PinInfoView", owner: self, options: nil)[0] as! PinInfoView
                customView.setLocationEntity(annotationView.annotation as! LocationEntity)
                snapshotView.addSubview(customView)
            }
        }
        
        if #available(iOS 9.0, *) {
            annotationView.detailCalloutAccessoryView = snapshotView
        } else {
            // Fallback on earlier versions
        }
    }
    
    func mapMoveAnimationWithMKAnnotationView(view: MKAnnotationView) -> CGRect {
        let dt: CGFloat = 0
        var viewRect = view.frame
        let height: CGFloat = PinInfoView.popupViewSize.height
        let oy = height + view.frame.size.height * 0.5 + dt
        let notMoveRect = CGRectMake(30, oy, CGRectGetWidth(self.mapviewMain.frame) - 60, CGRectGetHeight(self.mapviewMain.frame) - oy)
        if CGRectContainsRect(notMoveRect, viewRect) == false {
            let mx = CGRectGetMidX(viewRect)
            let my = CGRectGetMidY(viewRect)
            var dx: CGFloat = 0, dy: CGFloat = 0
            if mx > CGRectGetMaxX(notMoveRect) {
                dx = mx - CGRectGetMaxX(notMoveRect)
            }
            else if (mx < CGRectGetMinX(notMoveRect)) {
                dx = mx - CGRectGetMinX(notMoveRect)
            }
            
            if my > CGRectGetMaxY(notMoveRect) {
                dy = my - CGRectGetMaxY(notMoveRect)
            }
            else if (my < CGRectGetMinY(notMoveRect)) {
                dy = my - CGRectGetMinY(notMoveRect)
            }
            
            let point = CGPointMake(CGRectGetWidth(self.mapviewMain.frame) * 0.5 + dx, (CGRectGetHeight(self.mapviewMain.frame) - dt) * 0.5 + dy + dt)
            let coordinate = self.mapviewMain.convertPoint(point, toCoordinateFromView: self.mapviewMain)
            self.mapviewMain.setCenterCoordinate(coordinate, animated: true)
            viewRect = CGRectOffset(viewRect, -dx, -dy)
        }
        return viewRect
    }
    
    // MARK: - PinInfoView delegate
    func PinInfoViewDidShow(pinInfoView: PinInfoView) {
        
    }
    
    func PinInfoViewWillHide(pinInfoView: PinInfoView) {
        deselectAllAnnotation()
    }
    
    private func deselectAllAnnotation() -> Void {
        for annotation in self.mapviewMain.selectedAnnotations {
            self.mapviewMain.deselectAnnotation(annotation, animated: false)
        }
    }
    
    //MARK: button action

    @IBAction func backAction(sender: AnyObject) {
        
        timer.invalidate()
        self.navigationController?.popViewControllerAnimated(true)
    }
}
