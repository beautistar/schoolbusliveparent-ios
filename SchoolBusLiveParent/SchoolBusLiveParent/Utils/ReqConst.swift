//
//  ReqConst.swift
//  SchoolBusLiveParent
//
//  Created by AOC on 8/4/16.
//  Copyright © 2016 AOC. All rights reserved.
//

import Foundation

struct Req {
    
    static let BASE_URL = "http://www.schoolbuslive.com/iOS/DataAPI.ashx?do="
    
    static let LOGIN = "login"
    static let REGISTER = "register"
    static let FORGOT = "forgot"
    static let TRIPLIST = "triplist"
    
    
    //request params
    
    static let CGUID = "cguid"
    static let TGUID = "tguid"
    static let LOC = "loc"
    
    // request params
    
    static let EMAIL = "email"
    static let PWD = "pwd"
    static let FIRSTNAME = "firstname"
    static let LASTNAME = "lastname"
    static let GENDER = "gender"
    
    
    


}
